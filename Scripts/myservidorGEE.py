#------------------------------------------------------------
# @Author: Carlos_Enciso_Ojeda <usuario>
# @File: /home/cenciso/Downloads/conda_bash_2020.sh
# @Date:   2020-06-14T16:43:37-05:00
# @Email:  carlos.enciso.o@gmail.com
# @Created Date: Tuesday, Jule 20th 2020, 1:28:57 am
# --------
# @Last Modified: Friday, Jule 20th 2020 11:39:39 am
# @Modified By: Carlos Enciso Ojeda at <carlos.enciso.o@gmail.com>
# --------
# @License: MIT License
# Copyright (c) 2020 Peruvian Geophysical Institute
#------------------------------------------------------------
#-------------------------------------#
# Modules 
#-------------------------------------#
import matplotlib.pyplot as plt
import branca.colormap as cm
from folium import plugins
import geopandas as gpd
import xarray as xr
import pandas as pd
import numpy as np
import folium
import traceback
import ee
import os
#-------------------------------------#
# Main class 
#-------------------------------------#
class mypyearthexplore:
    def __init__(self, lat,lon, zoom=8):
        location = [lat,lon]
        self.Map = folium.Map(location=location, zoom_start=zoom)
        plugins.Fullscreen().add_to(self.Map)
        try:
            ee.Initialize()
        except:
            ee.Authenticate()
            ee.Initialize()
    def add_basemap(self, basemap=None):
        basemaps = {'Google Maps': folium.TileLayer(tiles='https://mt1.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', 
                                                    attr = 'Google', name = 'Google Maps', overlay = True, control=True), 
                    'Google Satellite': folium.TileLayer(tiles='https://mt1.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', 
                                                         attr = 'Google', name= 'Google Satellite', overlay=True, control=True), 
                    'Google Terrain': folium.TileLayer(tiles= 'https://mt1.google.com/vt/lyrs=p&x={x}&y={y}&z={z}', 
                                                       attr= 'Google', name= 'Google Terrain', overlay=True, control=True), 
                    'Google Satellite Hybrid': folium.TileLayer(tiles= 'https://mt1.google.com/vt/lyrs=y&x={x}&y={y}&z={z}', 
                                                                attr= 'Google', name= 'Google Hybrid', overlay=True, control=True)}
        basemaps[basemap].add_to(self.Map)
    @property
    def addLcontrol(self):
        self.Map.add_child(folium.LayerControl())
    def add_ee_layer(self, ee_object, vis_params, name, opacity=1.0):
        try:
            if isinstance(ee_object, ee.image.Image):
                map_id_dict =  ee.Image(ee_object).getMapId(vis_params)
                folium.raster_layers.TileLayer(tiles= map_id_dict['tile_fetcher'].url_format, attr='Google Earth Engine', 
                                               name=name, overlay=True, control=True, opacity=opacity).add_to(self.Map)
            elif isinstance(ee_object, ee.imagecollection.ImageCollection):
                ee_object_new = ee_object.mosaic()
                map_id_dict = ee.Image(ee_object_new).getMapId(vis_params)
                folium.raster_layers.TileLayer(tiles=map_id_dict['tile_fetcher'].url_format, attr='Google Earth Engine', 
                                               name=name, overlay=True, control=True, opacity=opacity).add_to(self.Map)
            elif isinstance(ee_object, ee.geometry.Geometry):
                folium.GeoJson(data=ee_object.getInfo(), name=name, overlay=True, control=True).add_to(self.Map)
            elif isinstance(ee_object, ee.featurecollection.FeatureCollection):
                ee_object_new = ee.Image().paint(ee_object,0,2)
                map_id_dict = ee.Image(ee_object_new).getMapId(vis_params)
                folium.raster_layers.TileLayer(tiles=map_id_dict['tile_fetcher'].url_format, attr='Google Earth Engine', 
                                               name=name, overlay=True, control=True).add_to(self.Map)
        except:
            traceback.print_exc()
    def adding_layers(self, product, varname, idate, fdate, vparam, name):
        self.varname = varname
        if fdate:
            self.product = (ee.ImageCollection(product).select(varname).filterDate(idate,fdate))
            self.add_ee_layer(self.product.mean(), vparam, name)
        else:
            self.product = (ee.ImageCollection(product).select(varname))
            self.add_ee_layer(self.product, vparam, name)
    def add_colorbar(self, vmin, vmax):
        self.colormap = cm.LinearColormap(colors=['blue', 'purple', 'cyan', 'green', 'yellow', 'red'], index=list(np.linspace(vmin,vmax,6)),vmin=vmin,vmax=vmax)
        self.colormap.caption = self.varname
        self.colormap.add_to(self.Map)

#-----------------------------------
# Is this running by its own?
#-----------------------------------
if __name__ == '__main__':
    pass
